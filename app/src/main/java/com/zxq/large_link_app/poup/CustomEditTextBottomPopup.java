package com.zxq.large_link_app.poup;

import android.content.Context;
import android.os.Handler;
import android.os.Looper;
import android.os.Message;
import android.view.View;
import android.widget.EditText;
import android.widget.Toast;

import androidx.annotation.NonNull;

import com.lxj.xpopup.core.BottomPopupView;
import com.zxq.large_link_app.R;
import com.zxq.large_link_app.enity.IotPlatform;
import com.zxq.large_link_app.enity.IotProject;
import com.zxq.large_link_app.enity.LocationInfo;
import com.zxq.large_link_app.http.HttpAddProject;

import java.util.Locale;
import java.util.concurrent.ArrayBlockingQueue;
import java.util.concurrent.ThreadPoolExecutor;
import java.util.concurrent.TimeUnit;


/**
 * Description: 自定义带有输入框的Bottom弹窗
 * Create by dance, at 2019/2/27
 */
public class CustomEditTextBottomPopup extends BottomPopupView {
    public CustomEditTextBottomPopup(@NonNull Context context) {
        super(context);
    }

    @Override
    protected int getImplLayoutId() {
        return R.layout.custom_edittext_bottom_popup;
    }

    /**
     * 线程池
     */
    ThreadPoolExecutor poolExecutor;

    @Override
    protected void onCreate() {
        super.onCreate();
        findViewById(R.id.btn_project_up).setOnClickListener(dismissClick);

    }

    View.OnClickListener dismissClick = new OnClickListener() {
        @Override
        public void onClick(View v) {
            reportProjectInfo();

        }
    };


    /**
     * 上报项目信息
     */
    private void reportProjectInfo() {
        String projectName = getEtText(R.id.input_project_name);
        String projectSite = getEtText(R.id.input_project_site);
        String projectPrincipal = getEtText(R.id.input_project_principal);
        String projectTel = getEtText(R.id.input_project_tel);
        String projectWarranty = getEtText(R.id.input_project_warranty);
        String remark = getEtText(R.id.input_project_remark);
        IotProject iotProject = new IotProject();
        iotProject.setName(projectName);
        iotProject.setPlace(projectSite);
        iotProject.setPrincipal(projectPrincipal);
        iotProject.setPrincipalTel(projectTel);
        if(!projectWarranty.isEmpty()) {
            iotProject.setWarrantyYear(Long.parseLong(projectWarranty));
        }
        iotProject.setRemark(remark);

        if(getLatitude() != null){
            iotProject.setLatitude(getLatitude());
        }
        if(getLongitude() != null) {
            iotProject.setLongitude(getLongitude());
        }
        poolExecutor.execute(new UpTask(iotProject));
    }

    Handler uiHandler = new Handler(Looper.getMainLooper()){

        @Override
        public void handleMessage(@NonNull Message msg) {
            super.handleMessage(msg);
            if(msg.what == 0x01) {
                Boolean rst = (Boolean) msg.obj;
                String msgTip = "";
                if(rst) {
                    msgTip = "添加成功";
                } else {
                    msgTip = "添加失败";
                }
                Toast.makeText(getContext(), msgTip, Toast.LENGTH_LONG).show();
                dismiss();
            }
            if(msg.what == 0x02) {
                if(getAddress() != null) {
                    setEtText(R.id.input_project_site, getAddress());
                }
            }

        }
    };


    private class UpTask implements Runnable {
        private IotProject iotProject;
        public UpTask(IotProject iotProject) {
            this.iotProject = iotProject;
        }

        @Override
        public void run() {
            Boolean rst = HttpAddProject.getInstance().addProjectInfo(iotProject);
            Message message = Message.obtain();
            message.what = 0x01;
            message.obj = rst;
            uiHandler.sendMessage(message);
        }
    };



    /**
     * 获取经度
     * @return
     */
    private Double getLongitude() {


        return LocationInfo.location == null?null:LocationInfo.location.getLongitude();
    }

    private String getAddress() {
        return LocationInfo.location == null?null:LocationInfo.location.getAddrStr();
    }

    /**
     * 获取维度
     * @return
     */
    private Double getLatitude() {

        return LocationInfo.location == null? null:LocationInfo.location.getLatitude();
    }

    public String getEtText(Integer rId) {
        EditText et =  findViewById(rId);
        String data = et.getText().toString().trim();
        return data;
    }


    public void setEtText(Integer rId,String value) {
        EditText et =  findViewById(rId);
        et.setText(value);
    }




    @Override
    protected void onShow() {
        super.onShow();
        poolExecutor = new ThreadPoolExecutor(2,4,1L, TimeUnit.HOURS,new ArrayBlockingQueue<>(1));
        poolExecutor.execute(new Runnable() {
            @Override
            public void run() {
                while (!Thread.interrupted()) {

                    try {
                        Thread.sleep(1000);
                        uiHandler.sendEmptyMessage(0x02);
                        Thread.sleep(30000);
                    } catch (InterruptedException e) {
                        e.printStackTrace();
                    }
                }
            }
        });
    }

    @Override
    protected void onDismiss() {
        super.onDismiss();
        if(poolExecutor != null) {
            poolExecutor.shutdown();
            poolExecutor = null;
        }
    }



}
