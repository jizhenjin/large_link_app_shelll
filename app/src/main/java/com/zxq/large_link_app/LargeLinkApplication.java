package com.zxq.large_link_app;

import android.app.Application;

import com.zxq.large_link_app.sp.SpDBLite;

public class LargeLinkApplication extends Application {

    @Override
    public void onCreate() {
        super.onCreate();
        SpDBLite.config(this);
    }
}
