package com.zxq.large_link_app.http;

import android.util.Log;

import com.alibaba.fastjson.JSON;
import com.alibaba.fastjson.JSONException;
import com.zxq.large_link_app.enity.IotPlatform;
import com.zxq.large_link_app.http.listen.ISwitchPlatformListener;
import com.zxq.large_link_app.sp.SpDBLite;

import okhttp3.OkHttpClient;
import okhttp3.Request;

import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

public class HttpSwitchPlatform {

    /**
     * 平台地址
     */
    static final String platformUrl = "http://sf.zxqnywlw.cn:8080/iot/platform/total";

    static ISwitchPlatformListener iSwitchPlatformListener;

    /**
     * 设置平台切换监听
     * @param iSwitchPlatformListener
     */
    public static void setISwitchPlatformListener(ISwitchPlatformListener iSwitchPlatformListener){
        HttpSwitchPlatform.iSwitchPlatformListener = iSwitchPlatformListener;
    }

    public static ISwitchPlatformListener getISwitchPlatformListener(){
        return iSwitchPlatformListener;
    }



    /**
     * 获取全部的平台信息
     * @return
     */
    public static List<IotPlatform> getTotalPlatformMessage() {
        OkHttpClient client = new OkHttpClient();

        Request request = new Request.Builder()
                .url(platformUrl)
                .build();
        String result = null;
        try {
            result = client.newCall(request).execute().body().string();
            if(result != null){
                List<IotPlatform> iotPlatformList = JSON.parseArray(result, IotPlatform.class);
                return iotPlatformList;
            }
        } catch (IOException e) {
            e.printStackTrace();
        }catch (JSONException je) {
            je.printStackTrace();
        }
        IotPlatform iotPlatform = new IotPlatform();
        iotPlatform.setCompanyName("三丰--默认平台");
        Log.e("h5Url", SpDBLite.getObj().getH5Url() + "");
        iotPlatform.setH5Url(SpDBLite.getObj().getH5Url());
        iotPlatform.setWebUrl(SpDBLite.getObj().getWebJoint());
        List<IotPlatform> preH5 = new ArrayList<>();
        preH5.add(iotPlatform);
        return preH5;
    }



}
