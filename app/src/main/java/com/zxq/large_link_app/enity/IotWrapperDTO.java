package com.zxq.large_link_app.enity;


import java.io.Serializable;


public class IotWrapperDTO<T> implements Serializable {

    T data;

    public void setData(T data) {
        this.data = data;
    }

    public T getData() {
        return data;
    }

}
