package com.zxq.large_link_app.enity;

import com.github.gzuliyujiang.wheelview.contract.TextProvider;

import java.util.ArrayList;
import java.util.List;

public class NavMapEntity implements TextProvider {

    private String mapName;
    static List<TextProvider> displayMap = new ArrayList<>();

    public static List<TextProvider> getDisplayMap (){
        displayMap.clear();
        displayMap.add(new NavMapEntity("百度地图"));
        displayMap.add(new NavMapEntity("高德地图"));
        return displayMap;
    }

    public NavMapEntity(String mapName) {

        this.mapName = mapName;
    }


    @Override
    public String provideText() {

        return mapName;
    }



}
