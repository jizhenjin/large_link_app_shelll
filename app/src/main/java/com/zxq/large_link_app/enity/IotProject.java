package com.zxq.large_link_app.enity;


import java.util.Date;

/**
 * 项目信息对象 iot_project
 * 
 * @author ruoyi
 * @date 2022-11-08
 */
public class IotProject
{
    private static final long serialVersionUID = 1L;




    private String name;

    private String place;


    private Double longitude;


    private Double latitude;


    private String principal;


    private String principalTel;


    private String startupTime;

    /** 状态（立项，施工，竣工，验收，结束） */
    private Long state;

    /** 质保年份 */
    private Long warrantyYear;

    private String remark;

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getPlace() {
        return place;
    }

    public void setPlace(String place) {
        this.place = place;
    }

    public Double getLongitude() {
        return longitude;
    }

    public void setLongitude(Double longitude) {
        this.longitude = longitude;
    }

    public Double getLatitude() {
        return latitude;
    }

    public void setLatitude(Double latitude) {
        this.latitude = latitude;
    }

    public String getPrincipal() {
        return principal;
    }

    public void setPrincipal(String principal) {
        this.principal = principal;
    }

    public String getPrincipalTel() {
        return principalTel;
    }

    public void setPrincipalTel(String principalTel) {
        this.principalTel = principalTel;
    }

    public String getStartupTime() {
        return startupTime;
    }

    public void setStartupTime(String startupTime) {
        this.startupTime = startupTime;
    }

    public Long getState() {
        return state;
    }

    public void setState(Long state) {
        this.state = state;
    }

    public Long getWarrantyYear() {
        return warrantyYear;
    }

    public void setWarrantyYear(Long warrantyYear) {
        this.warrantyYear = warrantyYear;
    }

    public String getRemark() {
        return remark;
    }

    public void setRemark(String remark) {
        this.remark = remark;
    }

    @Override
    public String toString() {
        return
                "name=" + name + '&' +
                "place=" + place + '&' +
                "longitude=" + longitude + '&' +
                "latitude=" + latitude + '&' +
                "principal=" + principal + '&' +
                "principalTel=" + principalTel + '&' +
                "startupTime=" + startupTime + '&' +
                "state=" + state + '&' +
                "warrantyYear=" + warrantyYear + '&' +
                "remark=" + remark;
    }
}
