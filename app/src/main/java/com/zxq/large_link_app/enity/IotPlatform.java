package com.zxq.large_link_app.enity;

import com.github.gzuliyujiang.wheelview.contract.TextProvider;

import java.io.Serializable;
import java.util.Date;

/**
 * 平台信息对象 iot_platform
 * 
 * @author ruoyi
 * @date 2022-12-08
 */
public class IotPlatform implements Serializable , TextProvider
{
    private static final long serialVersionUID = 1L;

    /** 主键 */

    private Long id;

    /** 公司名称 */

    private String companyName;

    /** h5地址 */

    private String h5Url;

    /** 路径地址 */

    private String webUrl;

    /** 用户名 */

    private String user;

    /** 密码 */

    private String password;



    public void setId(Long id) 
    {
        this.id = id;
    }

    public Long getId() 
    {
        return id;
    }
    public void setCompanyName(String companyName) 
    {
        this.companyName = companyName;
    }

    public String getCompanyName() 
    {
        return companyName;
    }
    public void setH5Url(String h5Url) 
    {
        this.h5Url = h5Url;
    }

    public String getH5Url() 
    {
        return h5Url;
    }
    public void setWebUrl(String webUrl) 
    {
        this.webUrl = webUrl;
    }

    public String getWebUrl() 
    {
        return webUrl;
    }
    public void setUser(String user) 
    {
        this.user = user;
    }

    public String getUser() 
    {
        return user;
    }
    public void setPassword(String password) 
    {
        this.password = password;
    }

    public String getPassword() 
    {
        return password;
    }

    @Override
    public String provideText() {
        return companyName;
    }
}
