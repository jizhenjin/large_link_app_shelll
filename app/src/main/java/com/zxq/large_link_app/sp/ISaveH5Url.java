package com.zxq.large_link_app.sp;

public interface ISaveH5Url {

    /**
     * 存储h5地址
     * @param url
     */
    void saveBaseUrl(String url);

    /**
     * 获取H5地址
     * @return
     */
    String getH5Url();

    /**
     * 获取Web端口地址
     * @return
     */
    String getWebJoint();


}
